var bgs = []

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}
bgs.push('url("../../img/backgrounds/tineo.jpeg")')
bgs.push('url("../../img/backgrounds/city.jpg")')
bgs.push('url("../../img/backgrounds/city2.jpg")')
bgs.push('url("../../img/backgrounds/sky1.jpg")')
bgs.push('url("../../img/backgrounds/brooklyn-bridge.jpg")')

shuffle(bgs)

setInterval (function (){

	var currentBg = bgs.shift();
	bgs.push(currentBg);
	$("body").css('background-image', currentBg);
}, 10000)


