// loads various gulp modules
var gulp = require('gulp');
var concat = require('gulp-concat');
var cleancss = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

// Minify CSS
gulp.task('css', done => {
    gulp.src('src/theme/css/src/*.css')
        .pipe(cleancss())
        .pipe(concat('style.min.css'))
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('src/theme/css/dist/'));
    done();
});

// Minify JS
gulp.task('js', done => {
  gulp.src('src/theme/js/src/*.js')
    .pipe(uglify())
	.pipe(concat('all.js'))
    .pipe(gulp.dest('src/theme/js/dist'))
    done();
});
