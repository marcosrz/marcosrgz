var path = require('path');
var express = require('express');
var compression = require('compression');

process.env.NODE_ENV = 'production'

var app = express();
var port = (process.argv[2] || 8080);
var rootPath = (process.argv[3] || path.join(__dirname, 'dist'));



// Middleware for compression
app.use(compression());
// Set document root
app.use(express.static(rootPath));

// app.use(compression({
//   threshold: 1024, // Solo comprimir respuestas de más de 1KB
//   filter: (req, res) => {
//     // Función personalizada para decidir si comprimir o no
//     if (req.headers['x-no-compression']) {
//       return false; // No comprimir si el encabezado `x-no-compression` está presente
//     }
//     return compression.filter(req, res); // Usa la lógica predeterminada para decidir
//   }
// }));

// Serve robots.txt explicitly
app.get('/robots.txt', function(req, res) {
  res.sendFile(path.join(rootPath, 'robots.txt'));
});

// [SEO] Hide server tech
app.disable('x-powered-by');

// Wake up!
app.listen(port, () => {
  console.log("-----------------------");
  console.log(" https://marcosrgz.com ");
  console.log("-----------------------");
  console.log("Server up and running !!");
  console.log("Port: " + port);
  console.log("Root: " + rootPath);
  console.log("----------------------");
});
