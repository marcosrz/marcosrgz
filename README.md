# MarcosRGZ

## PARCEL

New build under construction using parcel

### Requirements

- Node.js v20+ (Not sure what versions lower than that work)



## Build

Build css and js ```npm run build```

# Lithium - Ultra-light web server 

[![Build Status](https://travis-ci.org/MarcosRZ/lithium-web-server.svg?branch=devel)](https://travis-ci.org/MarcosRZ/lithium-web-server)

![lithium-web-server.js logo](http://telesucaro.es/lithium-webserver-sm.png)

## Install
Install the required packages with the following command:

```
npm install
```

## Run the server
The following command will run Lithium on your system.

```
node lithium.js
```

By default, lithium runs at port 8080, and the root directory of the content is
"./public".

## Change port and root
To change the port and the root dir, just call Lithium with the desired config as parameter.

```
node lithium.js 3333 /my/awesome/dir
```
