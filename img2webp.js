const sharp = require('sharp');
const path = require('path');
const fs = require('fs');

// Obtener el archivo desde los argumentos de línea de comandos
const inputFile = process.argv[2];

if (!inputFile) {
  console.error('Por favor, proporciona un archivo como argumento.');
  process.exit(1);
}

// Verificar que el archivo existe
if (!fs.existsSync(inputFile)) {
  console.error('El archivo especificado no existe.');
  process.exit(1);
}

// Obtener el directorio y el nombre base del archivo
const directory = path.dirname(inputFile);
const fileName = path.basename(inputFile, path.extname(inputFile));

// Crear el nombre del archivo de salida con extensión .webp
const outputFile = path.join(directory, `${fileName}.webp`);

// Convertir la imagen a WebP
sharp(inputFile)
  .toFormat('webp')
  .toFile(outputFile, (err, info) => {
    if (err) {
      console.error('Error al convertir la imagen:', err);
      process.exit(1);
    } else {
      console.log(`Imagen convertida con éxito: ${outputFile}`);
    }
  });
